class EventEmitter {
  constructor() {
    this._events = {};
  }

  on(evt, listener) {
    (this._events[evt] || (this._events[evt] = [])).push(listener);
    return this;
  }

  emit(evt, arg) {
    (this._events[evt] || []).slice().forEach(lsn => lsn(arg));
  }
}

class Model extends EventEmitter {
  constructor(options = {}) {
    super();
    this._route = options.route;
    this._routes = {
      '': 'renderTemplateMain',
      catalog: 'renderTemplateCatalog',
      favorites: 'renderTemplateFavorites',
    };
    this._urlApi = 'https://json.medrating.org';
    this._favoritePhotos = JSON.parse(
      localStorage.getItem('favoritePhotos')) || [];
  }

  fetchUsersAll() {
    return fetch(`${this._urlApi}/users/`)
      .then((res) => res.json());
  }

  fetchAlbumsByUserId(id) {
    return fetch(`${this._urlApi}/albums?userId=${id}`)
      .then((res) => res.json());
  }

  fetchPhotosByAlbumId(id) {
    return fetch(`${this._urlApi}/photos?albumId=${id}`)
      .then((res) => res.json());
  }

  fetchPhotosByIds(arrIds) {
    return Promise.all(arrIds.map((id) => {
      return fetch(`${this._urlApi}/photos/${id}`)
        .then((res) => res.json());
    }));
  }

  toggleFavoritePhoto(id) {
    const index = this._favoritePhotos.indexOf(id);

    if (index > -1) {
      this._favoritePhotos.splice(index, 1);
    } else {
      this._favoritePhotos.push(id);
    }

    localStorage.setItem('favoritePhotos',
      JSON.stringify(this._favoritePhotos));
    this.emit('toggleFavoritePhoto', id);
  }

  get favoritePhotos() {
    return this._favoritePhotos;
  }

  get routes() {
    return this._routes;
  }

  get route() {
    return this._route;
  }

  set route(route) {
    const previousRoute = this._route;
    this._route = route;
    this.emit('changedRoute', previousRoute);
  }
}

class View extends EventEmitter {
  constructor(model, options = {}) {
    super();
    this._model = model;
    this._appContainer = options.appContainer;
    this._routeButtons = options.routeButtons;
    this.toggleAlbums = this.toggleAlbums.bind(this);
    this.toggleFavoritePhoto = this.toggleFavoritePhoto.bind(this);

    model.on('changedRoute', () => {
      window.location.hash = `#${this._model.route}`;
    });

    model.on('toggleFavoritePhoto', this.toggleFavoritePhoto);

    window.addEventListener('hashchange', (e) => {
      const oldUrl = new URL(e.oldURL);
      const oldRoute = oldUrl.hash.slice(1);
      const newRoute = window.location.hash.slice(1);
      this.emit('hashChange', newRoute, oldRoute);
    });

    this._routeButtons.forEach((button) => {
      button.addEventListener('click', (e) => {
        const { routeTo } = e.target.dataset;
        this.emit('clickGoToRoute', routeTo);
      });
    });
  }

  renderTemplateMain() {
    this._appContainer.innerHTML = '';
  }

  renderTemplateDefault() {
    this._appContainer.innerHTML = '<div class="container">Page not found!</div>';
  }

  renderTemplateCatalog() {
    this._model.fetchUsersAll().then((users) => {
      let userListItems = '';

      users.forEach((user) => {
        if (!user.name) return;

        userListItems += `
          <li class="accordion__list-item">
            <button type="button" class="accordion__button button" data-user-id="${user.id}">${user.name}</button>
            <div class="accordion__panel"></div>
          </li>
        `;
      });

      this._appContainer.innerHTML = `
        <div class="container">
          <div class="accordion">
            <ul class="accordion__list list">
              ${userListItems}
            </ul>
          </div>
        </div>
      `;
    });
  }

  renderTemplateFavorites() {
    const { favoritePhotos } = this._model;

    this._model.fetchPhotosByIds(favoritePhotos).then((photos) => {
      let photoListItems = '';

      photos.forEach((photo) => {
        photoListItems += `
          <li class="album__list-item">
            <div class="photo">
              <div class="photo__inner">
                <div class="photo__actions">
                  <button type="button"
                    class="button favorites-button favorites-button--active"
                    data-favorite-id="${photo.id}">
                  </button>
                </div>
                <div class="photo__body">
                  <a href="${photo.url}" class="photo__link" title="${photo.title}" data-popup-button>
                    <img class="photo__image"
                       width="150"
                       height="150"
                       src="${photo.thumbnailUrl}"
                       alt="${photo.title}"
                    >
                  </a>
                </div>
              </div>
            </div>
          </li>
        `;
      });

      this._appContainer.innerHTML = `
        <div class="container">
          <div class="album">
            <div class="album__inner">
              <ul class="album__list list">
                ${photoListItems}
              </ul>
            </div>
          </div>
        </div>
      `;
    });
  }

  toggleFavoritePhoto(id) {
    const favoriteButton = document.querySelector(`[data-favorite-id="${id}"]`);
    favoriteButton.classList.toggle('favorites-button--active');

    const { route } = this._model;
    if (route === 'favorites') {
      const photoElement = favoriteButton.closest('.album__list-item');
      photoElement.remove();
    }
  }

  toggleAlbums(userButton) {
    const panel = userButton.nextElementSibling;
    const isActive = userButton.classList.contains(
      'accordion__button--active');

    if (isActive) {
      userButton.classList.remove('accordion__button--active');
      panel.innerHTML = '';
      return;
    }

    const { userId } = userButton.dataset;

    this._model.fetchAlbumsByUserId(userId).then((albums) => {
      let albumListItems = '';

      albums.forEach((album) => {
        albumListItems += `
          <li class="accordion__list-item">
            <button type="button" class="accordion__button button" data-album-id="${album.id}">${album.title}</button>
            <div class="accordion__panel"></div>
          </li>
        `;
      });

      panel.innerHTML = `
        <ul class="accordion__list list">
            ${albumListItems}
        </ul>
      `;

      userButton.classList.add('accordion__button--active');
    });
  }

  togglePhotos(albumButton) {
    const panel = albumButton.nextElementSibling;
    const isActive = albumButton.classList.contains(
      'accordion__button--active');

    if (isActive) {
      albumButton.classList.remove('accordion__button--active');
      panel.innerHTML = '';
      return;
    }

    const { albumId } = albumButton.dataset;

    this._model.fetchPhotosByAlbumId(albumId).then((photos) => {
      const { favoritePhotos } = this._model;
      let photoListItems = '';

      photos.forEach((photo) => {
        const isFavorite = favoritePhotos.indexOf(photo.id) > -1;

        photoListItems += `
          <li class="album__list-item">
            <div class="photo">
              <div class="photo__inner">
                <div class="photo__actions">
                  <button type="button"
                    class="button favorites-button
                        ${isFavorite ? 'favorites-button--active' : ''}"
                    data-favorite-id="${photo.id}">
                  </button>
                </div>
                <div class="photo__body">
                  <a href="${photo.url}" class="photo__link" title="${photo.title}" data-popup-button>
                    <img class="photo__image"
                       width="150"
                       height="150"
                       src="${photo.thumbnailUrl}"
                       alt="${photo.title}"
                    >
                  </a>
                </div>
              </div>
            </div>
          </li>
        `;
      });

      panel.innerHTML = `
        <div class="album">
          <div class="album__inner">
            <ul class="album__list list">
              ${photoListItems}
            </ul>
          </div>
        </div>
      `;

      albumButton.classList.add('accordion__button--active');
    });
  }

  createPopup(e) {
    e.preventDefault();

    const popupButton = e.target.closest('[data-popup-button]');
    const { href } = popupButton;
    const { alt } = popupButton.firstElementChild;

    this.showPopup(`<img src="${href}" alt="${alt}" title="${alt}">`);

    const popup = document.getElementById('popup');
    popup.addEventListener('click', (e) => {
      const overlay = e.target.classList.contains('popup__overlay');

      if (overlay) this.hidePopup(popup);
    });
  }

  showPopup(innerHtml) {
    document.body.style.overflow = 'hidden';

    this._appContainer.insertAdjacentHTML('beforeend', `
      <div id="popup" class="popup">
        <div class="popup__overlay">
            ${innerHtml}
        </div>
      </div>
    `);
  }

  hidePopup(popup) {
    document.body.style.overflow = '';
    popup.remove();
  }

  get appContainer() {
    return this._appContainer;
  }
}

class Controller {
  constructor(model, view) {
    this._model = model;
    this._view = view;
    this.goToRoute = this.goToRoute.bind(this);

    view.on('clickGoToRoute', this.goToRoute);
    view.on('hashChange', this.goToRoute);

    view.appContainer.addEventListener('click', (e) => {
      const userButton = e.target.closest('[data-user-id]');
      const albumButton = e.target.closest('[data-album-id]');
      const popupButton = e.target.closest('[data-popup-button]');
      const favoriteButton = e.target.closest('[data-favorite-id]');

      if (userButton) view.toggleAlbums(userButton);
      if (albumButton) view.togglePhotos(albumButton);
      if (popupButton) view.createPopup(e);
      if (favoriteButton) {
        const { favoriteId } = favoriteButton.dataset;
        model.toggleFavoritePhoto(+favoriteId);
      }
    });
  }

  goToRoute(newRoute) {
    const currentRoute = this._model.route;

    if (newRoute !== currentRoute) {
      this._model.route = newRoute;
      this.renderTemplate();
    }
  }

  renderTemplate() {
    const { routes, route } = this._model;
    const templateName = routes[route];
    const renderTemplate = this._view[templateName];

    if (renderTemplate) {
      renderTemplate.call(this._view);
    } else {
      this._view.renderTemplateDefault();
    }
  }

  init() {
    this.renderTemplate();
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const model = new Model({
    route: window.location.hash.slice(1),
  });
  const view = new View(model, {
    appContainer: document.getElementById('app'),
    routeButtons: document.querySelectorAll('[data-route-to]'),
  });
  const controller = new Controller(model, view);

  controller.init();
});
